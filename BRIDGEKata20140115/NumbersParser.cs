﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework.Constraints;

namespace BRIDGEKata20140115
{
    public class NumbersParser
    {
        private readonly DigitParser _digitParser = new DigitParser();

        public string ParseNumber(string input)
        {
            var number = string.Join("", this.SplitToDigits(input).Select(_digitParser.ParseDigit));

            if (!NumbersValidator.IsValid(number))
            {
                number += " ERR";
            }

            return number;
        }

        public IList<string> SplitToDigits(string input)
        {
            var result = new List<string>();

            for (int i = 0; i < 9; i++)
            {
                result.Add(string.Join("", input.Split('\n')
                    .Select(d => d.Substring(i*3, 3))));
            }

            return result;
        }
    }
}