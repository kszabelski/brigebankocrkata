﻿using NUnit.Framework;

namespace BRIDGEKata20140115
{
    public class NumbersValidatorTests
    {
        [TestCase("000000000", Result = true)]
        [TestCase("123456789", Result = false)]
        public bool IsValid_ShouldValidate(string input)
        {
            return NumbersValidator.IsValid(input);
        }
    }
}