﻿using System.Collections.Generic;

namespace BRIDGEKata20140115
{
    public class DigitParser
    {
        private List<string> digits = new List<string>() { Digits.Digit0, Digits.Digit1, Digits.Digit2, Digits.Digit3, Digits.Digit4, Digits.Digit5, Digits.Digit6, Digits.Digit7, Digits.Digit8, Digits.Digit9};

        public string ParseDigit(string digit)
        {
            return this.digits.IndexOf(digit).ToString();
        }
    }
}