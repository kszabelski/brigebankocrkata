﻿using NUnit.Framework;

namespace BRIDGEKata20140115
{
    public class DigitParserTests
    {
        private NumbersParserTests _numbersParserTests;

        public DigitParserTests(NumbersParserTests numbersParserTests)
        {
            _numbersParserTests = numbersParserTests;
        }

        [TestCase(Digits.Digit0, Result = "0")]
        [TestCase(Digits.Digit1, Result = "1")]
        [TestCase(Digits.Digit2, Result = "2")]
        public string ParseDigit_ShouldParse(string input)
        {
            return new DigitParser().ParseDigit(input);
        }
    }
}