﻿namespace BRIDGEKata20140115
{
    public class Digits
    {
        public const string Digit0 = " _ " +
                                     "| |" +
                                     "|_|";

        public const string Digit1 = "   " +
                                     "  |" +
                                     "  |";

        public const string Digit2 = " _ " +
                                     " _|" +
                                     "|_ ";

        public const string Digit3 = " _ " +
                                     " _|" +
                                     " _|";

        public const string Digit4 = "   " +
                                     "|_|" +
                                     "  |";

        public const string Digit5 = " _ " +
                                     "|_ " +
                                     " _|";

        public const string Digit6 = " _ " +
                                     "|_ " +
                                     "|_|";

        public const string Digit7 = " _ " +
                                     "  |" +
                                     "  |";

        public const string Digit8 = " _ " +
                                     "|_|" +
                                     "|_|";

        public const string Digit9 = " _ " +
                                     "|_|" +
                                     " _|";
    }
}