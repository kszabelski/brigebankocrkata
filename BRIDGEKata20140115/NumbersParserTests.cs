﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BRIDGEKata20140115
{
    public class NumbersParserTests
    {
        private NumbersParser _numbersParser = new NumbersParser();
        private readonly NumbersValidatorTests _numbersValidatorTests = new NumbersValidatorTests();
        private readonly DigitParserTests _digitParserTests;

        public NumbersParserTests()
        {
            _digitParserTests = new DigitParserTests(this);
        }

        private const string Input123456789 = "    _  _     _  _  _  _  _ \n" +
                                              "  | _| _||_||_ |_   ||_||_|\n" +
                                              "  ||_  _|  | _||_|  ||_| _|";

        private const string Input888888888 = " _  _  _  _  _  _  _  _  _ \n" +
                                              "|_||_||_||_||_||_||_||_||_|\n" +
                                              "|_||_||_||_||_||_||_||_||_|";

        private const string Input000000000 = " _  _  _  _  _  _  _  _  _ \n" +
                                              "| || || || || || || || || |\n" +
                                              "|_||_||_||_||_||_||_||_||_|";


        [TestCase(Input000000000, Result = "000000000")]
        [TestCase(Input888888888, Result = "888888888 ERR")]
        [TestCase(Input123456789, Result = "123456789 ERR")]
        public string ParseNumber_ShouldParse(string input)
        {
            return _numbersParser.ParseNumber(input);
        }

        [TestCase(Input000000000, Result = Digits.Digit0)]
        [TestCase(Input123456789, Result = Digits.Digit1)]
        public string SplitToDigits_ShouldHandleFirstDigit(string input)
        {
            var digits = _numbersParser.SplitToDigits(input);
            return digits.First();
        }
        
        [Test]
        public void SplitToDigits_ShouldSplitNumber()
        {
            var digits = _numbersParser.SplitToDigits(Input000000000);
            CollectionAssert.AreEqual(Enumerable.Range(1, 9).Select(s => Digits.Digit0), digits);
        }
    }
}
