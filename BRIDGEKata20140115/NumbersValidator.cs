﻿using System.Globalization;
using System.Linq;

namespace BRIDGEKata20140115
{
    public class NumbersValidator
    {
        public static bool IsValid(string number)
        {
            var digits = number
                .Select(s => int.Parse(s.ToString(CultureInfo.InvariantCulture))).ToList();

            int result = 0;
            for (int i = 1; i <= 9; i++)
            {
                result += digits[i - 1]*i;
            }

            return (result % 11)  == 0;
        }
    }
}